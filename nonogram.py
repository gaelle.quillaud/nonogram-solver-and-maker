import numpy as np
import matplotlib.pyplot as plt
import time

def str_to_int(input_list):
    """Convert the list of constraints from a list of str to a list of lists of int
    Args:
        input_list: list of strings representing constraints
    
    Returns:
        output_list: list of lists of int, representing the different clues
    """
    
    output_list = []

    for item in input_list:
        if ' ' in item:
            # if more than one block in a clue
            numbers = [int(part) for part in item.split()]
            output_list.append(numbers)
        else:
            # if only one block
            output_list.append([int(item)])

    return output_list

def read_file(example_number):
    """Read the file and create two lists with its information: rows and columns
    Args:
        example_number: int specifying which example file to use
    
    Returns:
        rows, columns: row and column constraints
    """
    
    rows = []
    columns = []
    file_path = 'examples/example' + str(example_number)
    
    with open(file_path, 'r') as file:
        current_block = []
        for line in file:
            line = line.strip()
            if not line:  # check for the blank line separating row and column constraints
                if current_block:
                    # if the current block has lines, add it to the appropriate list
                    if not rows:
                        rows = current_block  # first block of lines is rows
                    else:
                        columns = current_block  # second block of lines is columns
                    current_block = []
            else:
                # add the line to the current block
                current_block.append(line)

        if current_block:
            columns = current_block

    return (str_to_int(rows), str_to_int(columns))

def find_solution(puzzle, steps, columns, rows, i, j):
    """Recursive function to find a solution for the puzzle
    
    Args:
        puzzle: 2D list of 1s (filled) and 0s (unfilled)
        steps: list that keeps track of the steps taken to solve the puzzle. Each step is represented as [value, i, j],
                     where 'value' is 1 (cell filled), 'i' is the row index, and 'j' is the column index
        columns: 2D list that represents the column constraints for the puzzle
        rows: 2D list that represents the row constraints for the puzzle
        i: current row index
        j: current column index
    
    Returns:
        1 if a solution is found
        0 if no solution is found
    """

    width = len(columns)
    height = len(rows)

    # if the last row (i == height) is reached, a solution is found
    if i == height:
        return 1

    # indices of the next cell to explore
    next_i = i + 1 if j + 1 == width else i
    next_j = (j + 1) % width
    
    # mark the current cell as filled (1) and record the step
    steps.append([1, i, j])  # [value, i, j]
    puzzle[i][j] = 1
    
    # if the configuration with the cell filled is valid, recursively explore the next cell
    if verify(puzzle, columns, rows, i, j):
        if find_solution(puzzle, steps, columns, rows, next_i, next_j):
            return 1
    
    # Backtrack: unmark the current cell
    puzzle[i][j] = 0
    
    # if the configuration with the cell unfilled is valid, recursively explore the next cell
    if verify(puzzle, columns, rows, i, j):
        if find_solution(puzzle, steps, columns, rows, next_i, next_j):
            return 1

    # no solution is found for this configuration
    return 0

def verify(puzzle, columns, rows, i, j):
    """Function to verify if the current row and column configurations are valid
    
    Args:
        puzzle: 2D list of 1s (filled) and 0s (unfilled)
        columns: 2D list that represents the column constraints for the puzzle
        rows: 2D list that represents the row constraints for the puzzle
        i: current row index
        j: current column index
    
    Returns:
        1 if the current row and column configurations are valid
        0 otherwise
    """

    width = len(columns)
    height = len(rows)
    
    # helper functions to retrieve cell values in the current row and column.
    def row_getter(idx):
        return puzzle[idx][j]
    
    def col_getter(idx):
        return puzzle[i][idx]
    
    return (
        verify_line(columns[j], height, i, row_getter)  # Verify the current column (columns[j])
        and verify_line(rows[i], width, j, col_getter) # Verify the current row (rows[i])
    )

def verify_line(clues, max_length, length, getter):
    """Function to verify if a row or column configuration is valid
    
    Args:
        clues: list of constraints for row or column
        max_length: maximum length (number of cells) in the row or column
        length: current length of the row or column configuration being checked
        getter: function that takes an index and returns the value of the cell at that index

    Returns:
        1 if the row or column configuration is valid
        0 if it's not
    """

    k = 0 # index for the constraints list
    acc = 0 # accumulator for consecutive filled cells
    is_last = False # to track if the previous cell was filled

    for i in range(length + 1):
        if getter(i): # if the cell at index 'i' is filled
            acc += 1
            if not is_last:
                # if the previous cell wasn't filled, check if all the clues are checked 
                if k >= len(clues):
                    return 0
            is_last = True # current cell is filled

        else: # if the cell at index 'i' is unfilled
            if is_last:
                # if the previous cell was filled, check if the count of consecutive marked cells matches the current clue 
                if clues[k] != acc:
                    return 0
                acc = 0
                k += 1 # check next clue
            is_last = False # current cell is unfilled

    if length == max_length - 1: # if the last cell in the row is reached, check that all clues are checked
        if is_last:
            return k == len(clues) - 1 and acc == clues[k]
        else:
            return k == len(clues)
    else:
        if is_last:
            return acc <= clues[k]
    
    return 1

def solve_puzzle(columns, rows):
    """Main function to solve the puzzle
    
    Args:
        columns: 2D list that represents the column constraints for the puzzle
        rows: 2D list that represents the row constraints for the puzzle
    
    Returns:
        None if no solution is found
        A list containing the solved puzzle state and the steps taken to solve it otherwise
    """
    
    width = len(columns)
    height = len(rows)
    
    puzzle = [[-1 for _ in range(width)] for _ in range(height)]
    steps = [] # list to keep track of the steps taken to solve the puzzle

    # find a solution for the puzzle starting from the top-left corner
    if find_solution(puzzle, steps, columns, rows, 0, 0):
        return puzzle
    
    return None # puzzle unsolvable

def display_puzzle(puzzle):
    """Displays the nonogram with gray (0.5) value for still undetermined squares, 
    black squares for filled cells and white squares for unfilled cells
    Args:
        puzzle: 2D list of 1s (filled) and 0s (unfilled)
    
    Returns:
        image: NumPy array representing the displayed image.
    """
    
    for i in range(len(puzzle)):
        for j in range(len(puzzle[0])):
            if puzzle[i][j] == -1:
                puzzle[i][j] = 0.5
    image = 1 - np.array(puzzle)
    
    plt.imshow(image, cmap='gray')
    plt.show()
    
    return image

if __name__ == "__main__":
    start_time = time.time()
    rows, columns = read_file(example_number = 5)

    solved_puzzle = solve_puzzle(columns, rows)
    
    end_time = time.time()
    print(f"\nExecution time: {end_time - start_time} seconds\n")

    #print(solved_puzzle)
    #display_puzzle(solved_puzzle)
