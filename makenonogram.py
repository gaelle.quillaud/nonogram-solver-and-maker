import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from skimage import filters, color
from skimage.morphology import dilation, rectangle

def pixelate_image(img, pixelation_factor):
    """Pixelate an image (divide size by the pixelation factor)"""
    # Calculate the dimensions for the pixelated image
    width, height = img.shape
    new_width = width // pixelation_factor
    new_height = height // pixelation_factor
    
    print("puzzle size: ", new_width, new_height)
    # Create a pixelated image by resizing. Using the nearest approximation make it pixelised
    pixelated_image = Image.fromarray((img) * 255).resize((new_width, new_height), Image.NEAREST)

    # Resize the pixelated image back to the original dimensions (for better quality when saving)
    pixelated_image = pixelated_image.resize((width, height), Image.NEAREST)

    return pixelated_image

def binarize(gray_scale_img):
    """Binarize the pixelated image using Otsu's thresholding method"""
    
    thresh = filters.threshold_otsu(np.array(gray_scale_img))
    binary_image = (np.array(gray_scale_img) > thresh) * 255
    
    return binary_image

def puzzle_to_constraints_lists(puzzle):
    puzzle = puzzle / np.max(puzzle) # to have values between 0 and 1
    rows = []
    columns = []
    
    for row in puzzle:
        nb = 0
        clue = []

        for pix in row:
            if pix == 1:
                nb += 1
            elif nb > 0:
                clue.append(nb)
                nb = 0
    
        if nb > 0: # last pixel
            clue.append(nb)
            
        if not clue: # nothing on the row
            clue = [0]

        rows.append(clue)

    for j in range(len(puzzle[0])):
        nb = 0
        clue = []

        for row in puzzle:
            if row[j] == 1:
                nb += 1
            elif nb > 0:
                clue.append(nb)
                nb = 0
    
        if nb > 0:
            clue.append(nb)
            
        if not clue:
            clue = [0]

        columns.append(clue)

    return(rows, columns)

def load_image(path): 
    """Load the image and convert it to grayscale"""
    
    image =  np.array(Image.open(path))
    if image.ndim > 2 : 
        image = color.rgb2gray(image)
    
    return image

def display_puzzle(puzzle):
    """Display the puzzle"""

    plt.imshow(puzzle, cmap='gray', aspect='auto')
    plt.axis('off')
    plt.show()
    return None

def save_puzzle(puzzle, path):
    """Save the puzzle as an image"""
    
    puzzle_image = Image.fromarray(puzzle)
    if path == "files/einstein.jpeg":
        puzzle_image.save('files/puzzle_einstein.png')
    else : 
        puzzle_image.save('files/puzzle_hummingbird.png')

    return None

def save_txt_puzzle(rows, columns, path):
    if path == "files/einstein.jpeg":
        file_name = "files/einstein.txt"
    else:
        file_name = "files/hummingbird.txt"
    with open(file_name, "w") as file:
        for i, clue in enumerate(rows):
            file.write(" ".join(map(str, clue)) + ("\n" if i < len(rows) - 1 else ""))

        file.write("\n\n")

        for i, clue in enumerate(columns):
            file.write(" ".join(map(str, clue)) + ("\n" if i < len(rows) - 1 else ""))
  
if __name__ == "__main__":
    
    #image_path = "files/hummingbird.jpeg" # Recommended pixelation factor: 4, rectangle element : (4, 3)
    image_path = "files/einstein.jpeg" # Recommended pixelation factor: 30, rectangle element : (8, 15)
 
    image = load_image(image_path)

    ## Step 1: Apply Sobel edge detection filter
    edge_sobel = filters.sobel(image)

    ## Step 2: Apply dilation to the binary images with a rectangle element
    if image_path == "files/hummingbird.jpeg":
        dilated_image = dilation(edge_sobel, rectangle(4, 3)) # CAN BE AJUSTED
    else: 
        dilated_image = dilation(edge_sobel, rectangle(8, 15)) # CAN BE AJUSTED

    ## Step 3: Pixelise and binarize the dilated image
    if image_path == "files/hummingbird.jpeg":
        pixelation_factor = 4 # CAN BE AJUSTED
    else:
        pixelation_factor = 30 # CAN BE AJUSTED
    
    pixelated_image = pixelate_image(dilated_image, pixelation_factor)
    binary_image = binarize(pixelated_image)

    ## Step 4: Display and save the nonogram image
    puzzle = np.invert(binary_image.astype(np.uint8))
    display_puzzle(puzzle)
    save_puzzle(puzzle, image_path)

    ## Step 5: Create the nonogram text
    rows, columns = puzzle_to_constraints_lists(puzzle=puzzle)
    save_txt_puzzle(rows, columns, image_path)
